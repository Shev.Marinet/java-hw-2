package AreaShooting;

public class Board {
    private final static int TARGET = 10;
    private final static int HITED_TARGET = 20;
    private final static int MISSED_TARGET = -10;
    private final int dimX;
    private final int dimY;
    private int[][] data;

    public Board(int dimX, int dimY) {
        this.dimX = dimX;
        this.dimY = dimY;
        this.data = new int[dimY][dimX];
    }
    public String element(Point p) {
        int val = get(p);
        if (val == HITED_TARGET) {
            return " X ";
        }
        if (val == MISSED_TARGET) {
            return " * ";
        }
        if (p.x() == 0) {
            return " "+ p.y() + " ";
        }
        if (p.y() == 0) {
            return " "+ p.x() + " ";
        }
        else {
            return " - ";
        }
    }

    void printMe() {
        for (int row = 0; row < dimY; row++) {
            for (int col = 0; col < dimX; col++) {
                Point p = new Point(col, row);
                System.out.printf(" " + element(p)+ " |");
            }
            System.out.println();
        }
        System.out.println();
    }

    int get(Point p) {
        return this.data[p.y()][p.x()];
    }

    void set(Point p, int val) {
        this.data[p.y()][p.x()] = val;
    }

    void setTarget(Point p) {
        set(p, TARGET);
    }

    void setHitedTarget(Point p) {
        set(p, HITED_TARGET);
    }

    void setMissedTarget(Point p) {
        set(p, MISSED_TARGET);
    }

    public static void main(String[] args) {
        Board b = new Board(6,6);
        b.printMe();
    }

}
